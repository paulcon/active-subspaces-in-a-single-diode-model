%%
% This script produces the figures for the manuscript 'Discovering an
% Active Subspace in a Single-Diode Solar Cell Model.' The outputs of the
% MATLAB model used to compute Pmax from the single-diode model are
% contained in the associated .mat file. Requests for the simulation codes
% for the single-diode model can be sent to Mark.Campanelli@nrel.gov.

%%
clear all; close all;

% Load the random points from the normalized domain [-1,1]^5 and the
% natural domain of the model. Also load bounds on the ranges of the
% variables for reference. 
%
% Load samples of Pmax and the finite difference approximations of Pmax
% computed with the MATLAB simulation of the single-diode model at each of
% the sampled inputs.
load('inputs_outputs.mat');
[M,m] = size(theta0);


%%
% Compute the eigenvalues and eigenvectors corresponding to
% the active subspace analysis. 

[~,Sig,W] = svd(dPmax,'econ');
evals = (1/M)*diag(Sig).^2;
W = W.*repmat(sign(W(1,:)),m,1);

%%
% Use a nonparametric bootstrap to assess the variability in the estimated
% eigenvalues and subspaces. 

nboot = 1000;
evals_boot = zeros(m,nboot);
se_boot = zeros(m-1,nboot);
ind_boot = randi(M,M,nboot);
W1_boot = zeros(m,nboot);
W2_boot = zeros(m,nboot);

for i=1:nboot
    [~,Sig0,W0] = svd(dPmax(ind_boot(:,i),:),'econ');
    W0 = W0.*repmat(sign(W0(1,:)),m,1);
    evals_boot(:,i) = (1/M)*diag(Sig0).^2;
    se = zeros(m-1,1);
    for j=1:m-1
        se(j) = norm(W0(:,1:j)'*W(:,j+1:end));
    end
    se_boot(:,i) = se;
    W1_boot(:,i) = W0(:,1);
    W2_boot(:,i) = W0(:,2);
end

evals_upper = zeros(m,1); 
evals_lower = zeros(m,1);
for i=1:m
    e = sort(evals_boot(i,:),'ascend');
    evals_upper(i) = e(ceil(0.995*nboot));
    evals_lower(i) = e(floor(0.005*nboot));
end

se_upper = zeros(m-1,1); 
se_lower = zeros(m-1,1);
se_mean = zeros(m-1,1);
for i=1:m-1
    se_mean(i) = mean(se_boot(i,:));
    e = sort(se_boot(i,:),'ascend');
    se_upper(i) = e(ceil(0.995*nboot));
    se_lower(i) = e(floor(0.005*nboot));
end

%%
% Figure 1(a)
axisfs = 14;
markersize = 12;
linewidth = 1;

figure; 
hl = semilogy(1:m,evals,'ko-',...
    'LineWidth',linewidth,'MarkerSize',markersize);
axis square; grid on;
set(gca,'FontSize',14);
ylim([1e-7 1e-2]);
xlabel('Index'); ylabel('Eigenvalues');
hold on;
hp = patch([(1:m)';flipud((1:m)')],...
    [evals_upper; flipud(evals_lower)],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
legend([hl;hp],'Est','BI','Location','NorthEast');
hold off;
set(gca,'XTick',1:m); xlim([1,m]);
print(sprintf('figs/evals'),'-dpdf','-painters','-r300');

% Figure 1(b)
figure;
hl = semilogy(1:m-1,se_mean,'ko-',...
    'MarkerSize',markersize,'LineWidth',linewidth);
axis square; grid on;
set(gca,'FontSize',14);
ylim([1e-3 2]);
xlabel('Subspace Dimension'); ylabel('Distance');
hold on;
hp = patch([(1:m-1)';flipud((1:m-1)')],...
    [se_upper; flipud(se_lower)],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
legend([hl;hp],'Est','BI','Location','NorthEast');
hold off;

set(gca,'XTick',1:m-1); xlim([1,m-1]);

print(sprintf('figs/se'),'-dpdf','-painters','-r300');

% Figure 2(a)
figure;
plot(1:m,W1_boot(:,1:1000),'-','color',[0.8 0.8 0.8],'LineWidth',1);
set(gca,'FontSize',14);
axis square; grid on;
set(gca,'XTick',1:5); xlabel('Parameter Index');
ylim([-1 1]);
hold on;
plot(1:5,W(:,1),'ko-','LineWidth',1,'MarkerSize',12);
hold off;
print('figs/w1','-dpdf','-painters','-r300');

% Figure 2(b)
figure;
plot(1:m,W2_boot(:,1:1000),'-','color',[0.8 0.8 0.8],'LineWidth',1);
set(gca,'FontSize',14);
axis square; grid on;
set(gca,'XTick',1:5); xlabel('Parameter Index');
ylim([-1 1]);
hold on;
plot(1:5,W(:,2),'ko-','LineWidth',1,'MarkerSize',12);
hold off;
print('figs/w2','-dpdf','-painters','-r300');

%
% Figure 3(a)
MM = 20; NN = 100;
Y1 = theta0(1:NN,:)*W1_boot(:,1:MM);
Y2 = theta0(1:NN,:)*W2_boot(:,1:MM);
F = repmat(Pmax(1:NN),1,MM);

close all;
figure(1); hold on;
plot(Y1,F,'o',...
    'color',[0.8 0.8 0.8],'MarkerSize',7,...
    'MarkerFaceColor',[0.8 0.8 0.8]);
plot(theta0(1:NN,:)*W(:,1),Pmax(1:NN),'ko',...
    'LineWidth',2,'MarkerSize',12,'MarkerFaceColor',[0.6 0.6 0.6]);
set(gca,'FontSize',14);
axis square; grid on;
xlabel('Active Variable 1');
ylabel('P_{max} (W)');
hold off;
print('figs/oned','-dpdf','-painters','-r300');

% Figure 3(b)
figure(2); hold on;
scatter(Y1(:),Y2(:),100,F(:),'filled');
colormap('Gray');
scatter(theta0(1:NN,:)*W(:,1),theta0(1:NN,:)*W(:,2),20,Pmax(1:NN),'fill','ko');
set(gca,'FontSize',14);
axis square; grid on;
xlabel('Active Variable 1'); 
ylabel('Active Variable 2');
xlim([-1.5 1.5]); ylim([-1.5 1.5]);
t = colorbar('peer',gca); caxis([0 0.19]);
set(gca,'FontSize',14);
hold off;
print('figs/twod','-dpdf','-painters','-r300');

