# README #

This bucket contains the MATLAB scripts used to generate the figures in our recent submission 'Discovering an Active Subspace in a Single-Diode Solar Cell Model.' Questions regarding the scripts can be sent to Paul Constantine at paul.constantine@mines.edu